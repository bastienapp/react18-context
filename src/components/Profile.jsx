import { useContext } from "react";
import LoginContext from "../context/LoginContext";

function Profile() {
  const { user, setUser } = useContext(LoginContext);

  function login() {
    setUser({
      name: "Brandon",
    });
  }

  function logout() {
    setUser(null);
  }

  return (
    <div className="Profile">
      <h1>My profile</h1>
      {user ? (
        <div>
          <h3>Welcome {user.name}!</h3>
          <button type="button" onClick={logout}>
            Logout
          </button>
        </div>
      ) : (
        <div>
          <input type="text" value="Brandon" disabled />
          <br />
          <input type="password" value="*************" disabled />
          <br />
          <button type="button" onClick={login}>
            Login
          </button>
        </div>
      )}
    </div>
  );
}

export default Profile;
