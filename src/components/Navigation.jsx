import { useContext } from "react";
import LoginContext from "../context/LoginContext";
import "./Navigation.css";

function Navigation() {
  const { user } = useContext(LoginContext);
  return (
    <ul className="Navigation">
      <li>
        <img src="https://via.placeholder.com/200x100" alt="Logo" />
      </li>
      <li>
        <h2>Awesome website!</h2>
      </li>
      <li>{user ? user.name : "Please log in"}</li>
    </ul>
  );
}

export default Navigation;
