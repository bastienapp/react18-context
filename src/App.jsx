import { useState } from "react";
import "./App.css";
import Navigation from "./components/Navigation";
import Profile from "./components/Profile";
import LoginContext from "./context/LoginContext";

function App() {
  const [user, setUser] = useState(null);

  return (
    <div className="App">
      <LoginContext.Provider value={{ user, setUser }}>
        <Navigation />
        <Profile />
      </LoginContext.Provider>
    </div>
  );
}

export default App;
